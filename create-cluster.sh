#! /bin/bash

### Create Networking on GCP
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Creating VPC, subnet and firewall rules" "----------"
scripts/create-networking.sh

### Creating compute instances
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Creating 3 Workers and 3 Controllers" "----------"
scripts/create-instances.sh

### Create Certificate Authority
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Certificate Authority" "----------"
scripts/create-ca.sh

### Create Admin certificate
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Admin certificate" "----------"
scripts/create-admin-crt.sh

### Create Worker certificates
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Worker certificates" "----------"
scripts/create-kubelet-crt.sh

### Create Kube controller certificate
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Kube Controller Manager certificate" "----------"
scripts/create-kube-controller-manager-crt.sh

### Create Kube proxy certificate
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Kube Proxy certificate" "----------"
scripts/create-kube-proxy-crt.sh

### Create Kube scheduler certificate
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Kube Scheduler certificate" "----------"
scripts/create-kube-scheduler-crt.sh

### Create Kube API certificate
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Kube API certificate" "----------"
scripts/create-kube-api-crt.sh

### Create service account certificate
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Generating Service Account certificate" "----------"
scripts/create-service-account-crt.sh

### Distribute certificates
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Distributing certificates" "----------"
scripts/distribute-certificates.sh

### Create Kubernetes configuration files
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Creating Kubernetes Configuration files" "----------"
scripts/create-kubeconfig-files.sh

### Distributing Kubernetes configuration files
scripts/distribute-kubeconfig-files.sh

### Create and distribute data encryption key
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Creating and distributing Data Encryption key" "----------"
scripts/create-data-encryption-key.sh

### Create ansible inventory file
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Creating Ansible inventory file" "----------"
scripts/create-ansible-inventory.sh

### Install python on instances for Ansible
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Installing python on instances" "----------"
scripts/install-python.sh

### Configure and start etcd-cluster
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Configuring and starting the etcd-cluster" "----------"
fish -c "ansible-playbook-docker kthw-playbook.yaml -i inventory"

### Provision and configure Kubernetes Frontend Load Balancer
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Provising and configuring Kubernetes Frontend Load Balancer" "----------"
scripts/provision-load-balancer.sh

### Provision the pod networking routes
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Provising Pod Networking routes" "----------"
scripts/provision-pod-networking-routes.sh

### Move cluster-specific files
printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Move all cluster-specific files to 'current-cluster' directory" "----------"
mv *.pem *.csr *.kubeconfig encryption-config.yaml inventory ca-config.json current-cluster

### All done
printf '\e[32;1m\n\n\n%s\n%s\n%s\n\n\e[0m' "----------" "ALL DONE!" "----------"
