#! /bin/bash

ZONES=(a b c)

REGION=$(fish -c "gcloud config get-value compute/region")
REGION="${REGION%%[[:cntrl:]]}"

for i in 0 1 2; do
  printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting controller-${i} and worker-${i}" "----------"
  fish -c "gcloud -q compute instances delete controller-${i} worker-${i} --zone ${REGION}-${ZONES[${i}]}"
done

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting forwarding-rule kubernetes" "----------"
fish -c "gcloud -q compute forwarding-rules delete kubernetes-forwarding-rule \
    --region ${REGION}"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting target-pool kubernetes" "----------"
fish -c "gcloud -q compute target-pools delete kubernetes-target-pool"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting http-health-checks kubernetes" "----------"
fish -c "gcloud -q compute http-health-checks delete kubernetes"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting public address" "----------"
fish -c "gcloud -q compute addresses delete kubernetes-the-hard-way"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting firewall-rules" "----------"
fish -c "gcloud -q compute firewall-rules delete \
  kubernetes-the-hard-way-allow-internal \
  kubernetes-the-hard-way-allow-external \
  kubernetes-the-hard-way-allow-health-check"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting routing-rules" "----------"
fish -c "gcloud -q compute routes delete \
  kubernetes-route-10-200-0-0-24 \
  kubernetes-route-10-200-1-0-24 \
  kubernetes-route-10-200-2-0-24"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting subnet" "----------"
fish -c "gcloud -q compute networks subnets delete kubernetes"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting VPC" "----------"
fish -c "gcloud -q compute networks delete kubernetes-the-hard-way"

printf '\e[31;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Deleting all generated files" "----------"
rm current-cluster/*
echo "All (other) generated files have been deleted."

printf '\e[31;1m\n\n\n%s\n%s\n%s\n\n\e[0m' "----------" "ALL HAS BEEN DELETED!" "----------"
