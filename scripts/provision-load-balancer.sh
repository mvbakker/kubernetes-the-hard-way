#! /bin/bash

REGION=$(fish -c "gcloud config get-value compute/region")
REGION="${REGION%%[[:cntrl:]]}"

ZONES=(a b c)

KUBERNETES_PUBLIC_ADDRESS=$(fish -c "gcloud compute addresses describe kubernetes-the-hard-way \
    --region ${REGION} \
    --format 'value(address)'")

echo ${KUBERNETES_PUBLIC_ADDRESS}

fish -c "gcloud compute http-health-checks create kubernetes \
    --description "Kubernetes Health Check" \
    --host "kubernetes.default.svc.cluster.local" \
    --request-path "/healthz""

fish -c "gcloud compute firewall-rules create kubernetes-the-hard-way-allow-health-check \
    --network kubernetes-the-hard-way \
    --source-ranges 209.85.152.0/22,209.85.204.0/22,35.191.0.0/16 \
    --allow tcp"

fish -c "gcloud compute target-pools create kubernetes-target-pool \
    --http-health-check kubernetes"

for i in 0 1 2; do
    fish -c "gcloud compute target-pools add-instances kubernetes-target-pool \
        --instances-zone=${REGION}-${ZONES[${i}]} --instances controller-${i}"
done

fish -c "gcloud compute forwarding-rules create kubernetes-forwarding-rule \
    --address ${KUBERNETES_PUBLIC_ADDRESS} \
    --ports 6443 \
    --region ${REGION} \
    --target-pool kubernetes-target-pool"
