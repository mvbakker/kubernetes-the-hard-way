#! /bin/bash

REGION=$(fish -c "gcloud config get-value compute/region")

KUBERNETES_PUBLIC_ADDRESS=$(fish -c "gcloud compute addresses describe kubernetes-the-hard-way \
  --region ${REGION} \
  --format 'value(address)'")
KUBERNETES_PUBLIC_ADDRESS="${KUBERNETES_PUBLIC_ADDRESS%%[[:cntrl:]]}"

kubectl config set-cluster kubernetes-the-hard-way \
  --certificate-authority=current-cluster/ca.pem \
  --embed-certs=true \
  --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443

kubectl config set-credentials admin \
  --client-certificate=current-cluster/admin.pem \
  --client-key=current-cluster/admin-key.pem

kubectl config set-context kubernetes-the-hard-way \
  --cluster=kubernetes-the-hard-way \
  --user=admin

kubectl config use-context kubernetes-the-hard-way