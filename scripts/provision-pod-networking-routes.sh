#! /bin/bash

# FORMAT='value[separator=" "](networkInterfaces[0].networkIP,metadata.items[0].value)'

# for instance in worker-0 worker-1 worker-2; do
#   fish -c "gcloud compute instances describe ${instance} \
#     --format '${FORMAT}'"
# done

for i in 0 1 2; do
  fish -c "gcloud compute routes create kubernetes-route-10-200-${i}-0-24 \
    --network kubernetes-the-hard-way \
    --next-hop-address 10.240.0.2${i} \
    --destination-range 10.200.${i}.0/24"
done
