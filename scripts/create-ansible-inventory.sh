#! /bin/bash
declare -a EXTERNAL_IP_CONTROLLER
declare -a EXTERNAL_IP_WORKER

for i in 0 1 2; do
  ZONE=$(fish -c "gcloud compute instances list | grep worker-${i} | awk '{print \$2}'")

  EXTERNAL_IP_WORKER[${i}]=$(fish -c "gcloud compute instances describe --zone ${ZONE} worker-${i} --format 'value(networkInterfaces[0].accessConfigs[0].natIP)'") 
  EXTERNAL_IP_WORKER[${i}]="${EXTERNAL_IP_WORKER[${i}]%%[[:cntrl:]]}"
  EXTERNAL_IP_CONTROLLER[${i}]=$(fish -c "gcloud compute instances describe --zone ${ZONE} controller-${i} --format 'value(networkInterfaces[0].accessConfigs[0].natIP)'")
  EXTERNAL_IP_CONTROLLER[${i}]="${EXTERNAL_IP_CONTROLLER[${i}]%%[[:cntrl:]]}"
done

cat > inventory <<EOF
[controllers]
${EXTERNAL_IP_CONTROLLER[0]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root
${EXTERNAL_IP_CONTROLLER[1]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root
${EXTERNAL_IP_CONTROLLER[2]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root

[controller-0]
${EXTERNAL_IP_CONTROLLER[0]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root

[workers]
${EXTERNAL_IP_WORKER[0]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root
${EXTERNAL_IP_WORKER[1]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root
${EXTERNAL_IP_WORKER[2]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root

[worker-0]
${EXTERNAL_IP_WORKER[0]} ansible_ssh_private_key_file=/root/.ssh/google_compute_engine ansible_user=root
EOF

# cat inventory-create | tr -d '\r' >> inventory

echo "Inventory file created"
