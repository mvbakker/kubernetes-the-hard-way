#! /bin/bash

{

REGION=$(fish -c "gcloud config get-value compute/region")

KUBERNETES_PUBLIC_ADDRESS=$(fish -c "gcloud compute addresses describe kubernetes-the-hard-way \
  --region ${REGION} \
  --format 'value(address)'")
KUBERNETES_PUBLIC_ADDRESS="${KUBERNETES_PUBLIC_ADDRESS%%[[:cntrl:]]}"

echo "The public address is ${KUBERNETES_PUBLIC_ADDRESS}"

cat > kubernetes-csr.json <<EOF
{
  "hosts": [
    "10.32.0.1",
    "10.240.0.10",
    "10.240.0.11",
    "10.240.0.12",
    "${KUBERNETES_PUBLIC_ADDRESS}",
    "127.0.0.1",
    "kubernetes",
    "kubernetes.default",
    "kubernetes.default.svc",
    "kubernetes.default.svc.cluster",
    "kubernetes.default.svc.cluster.local"
  ],
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "NL",
      "L": "Amsterdam",
      "O": "Kubernetes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Holland"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes

rm kubernetes-csr.json

}
