#! /bin/bash

for i in 0 1 2; do
  ZONE=$(fish -c "gcloud compute instances list | grep worker-${i} | awk '{print \$2}'")

  fish -c "gcloud compute ssh --zone ${ZONE} worker-${i} --command 'apt install -y python-minimal'"
  fish -c "gcloud compute ssh --zone ${ZONE} controller-${i} --command 'apt install -y python-minimal'"
done
