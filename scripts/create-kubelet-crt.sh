#! /bin/bash

for instance in worker-0 worker-1 worker-2; do

ZONE=$(fish -c "gcloud compute instances list | grep ${instance} | awk '{print \$2}'")
 
EXTERNAL_IP=$(fish -c "gcloud compute instances describe --zone ${ZONE} ${instance} \
  --format 'value(networkInterfaces[0].accessConfigs[0].natIP)'")
EXTERNAL_IP="${EXTERNAL_IP%%[[:cntrl:]]}"

INTERNAL_IP=$(fish -c "gcloud compute instances describe --zone ${ZONE} ${instance} \
  --format 'value(networkInterfaces[0].networkIP)'")
INTERNAL_IP="${INTERNAL_IP%%[[:cntrl:]]}"

echo ""
echo "-----"
echo "This is ${instance} zone: ${ZONE}"
echo "This is ${instance} external IP: ${EXTERNAL_IP}"
echo "This is ${instance} internal IP: ${INTERNAL_IP}"

cat > ${instance}-csr.json <<EOF
{
  "hosts": [
    "${instance}",
    "${EXTERNAL_IP}",
    "${INTERNAL_IP}"
  ],
  "CN": "system:node:${instance}",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "NL",
      "L": "Amsterdam",
      "O": "system:nodes",
      "OU": "Kubernetes The Hard Way",
      "ST": "Holland"
    }
  ]
}
EOF

cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -profile=kubernetes \
  ${instance}-csr.json | cfssljson -bare ${instance}

rm ${instance}-csr.json

done
