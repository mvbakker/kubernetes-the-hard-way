#! /bin/bash

printf '\e[32;1m\n%s\n%s\n%s\n\n\e[0m' "----------" "Distributing the Kubernetes Configuration files" "----------"

for i in 0 1 2; do
  fish -c "gcloud compute scp worker-${i}.kubeconfig kube-proxy.kubeconfig worker-${i}:~/"
  fish -c "gcloud compute scp admin.kubeconfig kube-controller-manager.kubeconfig kube-scheduler.kubeconfig controller-${i}:~/"
done

