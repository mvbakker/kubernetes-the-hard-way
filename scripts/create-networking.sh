#! /bin/bash

REGION=$(fish -c "gcloud config get-value compute/region")

# Create a VPC
fish -c "gcloud compute networks create kubernetes-the-hard-way --subnet-mode custom"

# Create a subnet called 'kubernetes'
fish -c "gcloud compute networks subnets create kubernetes \
  --network kubernetes-the-hard-way \
  --range 10.240.0.0/24"

# Create a firewall rule that allows internal communication across all protocols
fish -c "gcloud compute firewall-rules create kubernetes-the-hard-way-allow-internal \
  --allow tcp,udp,icmp \
  --network kubernetes-the-hard-way \
  --source-ranges 10.240.0.0/24,10.200.0.0/16"

# Create a firewall rule that allows external SSH, ICMP, and HTTPS
fish -c "gcloud compute firewall-rules create kubernetes-the-hard-way-allow-external \
  --allow tcp:22,tcp:6443,icmp \
  --network kubernetes-the-hard-way \
  --source-ranges 0.0.0.0/0"

# Create a static IP address
fish -c "gcloud compute addresses create kubernetes-the-hard-way \
  --region ${REGION}"
