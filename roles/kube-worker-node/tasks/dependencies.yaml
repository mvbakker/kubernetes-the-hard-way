---
- name: Create needed directories
  file:
    path: "{{ item }}"
    state: directory
  with_items:
    - /etc/containerd
    - /etc/cni/net.d
    - /opt/cni/bin
    - /var/lib/kubelet
    - /var/lib/kube-proxy
    - /var/lib/kubernetes
    - /var/run/kubernetes

- name: Move certificates
  copy:
    src: /root/{{ item }}.pem
    dest: /var/lib/kubelet/
    remote_src: true
  with_items:
    - "{{ ansible_facts['hostname'] }}-key"
    - "{{ ansible_facts['hostname'] }}"

- name: Move kubeconfigs
  copy:
    src: /root/{{ ansible_facts['hostname'] }}.kubeconfig
    dest: /var/lib/kubelet/kubeconfig
    remote_src: true

- name: Move kubeconfigs
  copy:
    src: /root/kube-proxy.kubeconfig
    dest: /var/lib/kube-proxy/kubeconfig
    remote_src: true

- name: Move certificate authority
  copy:
    src: /root/ca.pem
    dest: /var/lib/kubernetes/
    remote_src: true

- name: Install nginx
  apt:
    name: ['socat', 'conntrack', 'ipset']
    state: present

- name: Download the crictl Binary
  get_url:
    url: "https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.12.0/crictl-v1.12.0-linux-amd64.tar.gz"
    dest: /tmp/
    backup: yes

- name: Unarchive crictl tarball
  unarchive:
    src: /tmp/crictl-v1.12.0-linux-amd64.tar.gz
    dest: /usr/local/bin/
    remote_src: yes

- name: Download the runsc Binary
  get_url:
    url: "https://storage.googleapis.com/kubernetes-the-hard-way/runsc-50c283b9f56bb7200938d9e207355f05f79f0d17"
    dest: /usr/local/bin/runsc
    owner: root
    mode: 0755
    backup: yes

- name: Download the runc Binary
  get_url:
    url: "https://github.com/opencontainers/runc/releases/download/v1.0.0-rc5/runc.amd64"
    dest: /usr/local/bin/runc
    owner: root
    mode: 0755
    backup: yes
  
- name: Download the cni-plugin Binary
  get_url:
    url: "https://github.com/containernetworking/plugins/releases/download/v0.6.0/cni-plugins-amd64-v0.6.0.tgz"
    dest: /tmp/
    backup: yes

- name: Unarchive cni-plugin tarball
  unarchive:
    src: /tmp/cni-plugins-amd64-v0.6.0.tgz
    dest: /opt/cni/bin/
    remote_src: yes

- name: Download the containerd Binary
  get_url:
    url: "https://github.com/containerd/containerd/releases/download/v1.2.0-rc.0/containerd-1.2.0-rc.0.linux-amd64.tar.gz"
    dest: /tmp/
    backup: yes

- name: Unarchive containerd tarball
  unarchive:
    src: /tmp/containerd-1.2.0-rc.0.linux-amd64.tar.gz
    dest: /
    remote_src: yes

- name: Download the kubelet Binary
  get_url:
    url: "https://storage.googleapis.com/kubernetes-release/release/v1.12.0/bin/linux/amd64/kubelet"
    dest: /usr/local/bin/
    owner: root
    mode: 0755
    backup: yes

- name: Download the kubectl Binary
  get_url:
    url: "https://storage.googleapis.com/kubernetes-release/release/v1.12.0/bin/linux/amd64/kubectl"
    dest: /usr/local/bin/
    owner: root
    mode: 0755
    backup: yes

- name: Download the kube-proxy Binary
  get_url:
    url: "https://storage.googleapis.com/kubernetes-release/release/v1.12.0/bin/linux/amd64/kube-proxy"
    dest: /usr/local/bin/
    owner: root
    mode: 0755
    backup: yes