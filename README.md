# kubernetes-the-hard-way

### TooLong;Didn'tWrite

To create run:

```bash
./create-cluster.sh
```

To delete run:

```bash
scripts/clean-up.sh
```

### Prerequisites
- Authenticate to google
- Set a default region `gcloud config set compute/region <your-region>` (find the one with lowest latency using gcping.com)
- Make sure Ansible is installed (or container available)
- Make sure cfssl is installed `brew install cfssl`
- Make sure kubeclt is installed `brew install kubernetes-cli`
